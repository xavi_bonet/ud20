package com.c4.maven.ud20_maven.Ejercicio2;


import java.awt.event.*;
import javax.swing.*;

public class AplicacionGrafica extends JFrame{

	// Panel de la aplicacion
	private JPanel contentPane;
	
	public AplicacionGrafica() {
		
		// CONTENEDOR
		
		// Titulo de la ventana
		setTitle("Ejercicio 2");
		// Tama�o ventana (x, y, longitud, altura)
		setBounds(600, 300, 450, 125);
		// Cerrar la app al cerrar la ventana
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		

		// PANEL
		
		// Crear el panel
		contentPane = new JPanel();
		// Indicar el dise�o del panel
		contentPane.setLayout(null);
		// Asignar el panel a la ventana
		setContentPane(contentPane);
		
		
		// COMPONENTES
		
		// Crear componente JLabel
		JLabel etiqueta1 = new JLabel("Has pulsado: ");
		// Colocar el componente (x, y, longitud, altura)
		etiqueta1.setBounds(25,25,200,20);
		// A�adir el componente al panel
		contentPane.add(etiqueta1);
		
		// Crear componente JLabel
		JLabel etiqueta2 = new JLabel("");
		// Colocar el componente (x, y, longitud, altura)
		etiqueta2.setBounds(105,25,200,20);
		// A�adir el componente al panel
		contentPane.add(etiqueta2);
		
		// Crear componente JButton
		JButton btn1 = new JButton("Boton 1");
		// Colocar el componente (x, y, longitud, altura)
		btn1.setBounds(175,25,100,20);
		// A�adir el componente al panel
		contentPane.add(btn1);
		
		// Crear componente JButton
		JButton btn2 = new JButton("Boton 2");
		// Colocar el componente (x, y, longitud, altura)
		btn2.setBounds(295,25,100,20);
		// A�adir el componente al panel
		contentPane.add(btn2);
		
		
		// EVENTOS
		
		// Evento al pulsar el boton 1
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				etiqueta2.setText("Boton 1");
			}
		});
		
		// Evento al pulsar el boton 2
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				etiqueta2.setText("Boton 2");
			}
		});
		
		
		// Hacer la ventana visible
		setVisible(true);
		
		
		
	}
	
}
