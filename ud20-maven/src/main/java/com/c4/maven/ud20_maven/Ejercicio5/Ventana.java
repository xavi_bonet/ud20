package com.c4.maven.ud20_maven.Ejercicio5;
/*
 * @author David
 * 
 */

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.*;
import javax.swing.text.DefaultCaret;

public class Ventana extends JFrame{
	private JPanel contentPane; //Panel de la aplicaci�n
	
	public Ventana() {
		//Titulo de la ventana //Cordenadas x,y,longitud,altura //Accion por defecto al cerrar la ventana
		setTitle("ejercicio 5"); 		
		setBounds(400,200,450,220); 				
		setDefaultCloseOperation(EXIT_ON_CLOSE); 	
		
		//Creamos el panel, indicamos su diseño y lo asignamos a la ventana.
		contentPane = new JPanel();
		contentPane.setLayout(null);
		setContentPane(contentPane); 
		
		//Creamos un boton y lo añadimos a la ventana
		JButton btn1 = new JButton("Limpiar");
		btn1.setBounds(5,5,150,20);
		contentPane.add(btn1);
		
		JTextArea textArea = new JTextArea("");
		textArea.setFont(new Font("Arial", Font.PLAIN, 16));
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setBounds(0,30,5000,50000);
		contentPane.add(textArea);
		
		//Hacemos visible la ventana
		setVisible(true); 

	   
		textArea.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent e) {
				textArea.append("Mouse clicked \n");
				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				textArea.append("Mouse entered \n");
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				textArea.append("Mouse exited \n");
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				textArea.append("Mouse pressed \n");
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				textArea.append("Mouse released \n");
				
			}
			
		});
		
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea.setText("");
			}
		});
		
		
	}
	
	public void iniciar() {
		System.out.println("Hola Mundo!");
	}
	
	

}
