package com.c4.maven.ud20_maven.Ejercicio4;

import java.awt.event.*;
import javax.swing.*;

public class AplicacionGrafica extends JFrame implements WindowListener {

	// Panel de la aplicacion
	private JPanel contentPane;
	private JTextArea textArea;

	public AplicacionGrafica() {

		// CONTENEDOR

		// Titulo de la ventana
		setTitle("Ejercicio 4");
		// Tama�o ventana (x, y, longitud, altura)
		setBounds(600, 300, 500, 500);
		// Cerrar la app al cerrar la ventana
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

		// PANEL

		// Crear el panel
		contentPane = new JPanel();
		// Indicar el dise�o del panel
		contentPane.setLayout(null);
		// Asignar el panel a la ventana
		setContentPane(contentPane);

		// COMPONENTES

		// Crear componente JLabel
		JLabel etiqueta1 = new JLabel("Eventos: ");
		// Colocar el componente (x, y, longitud, altura)
		etiqueta1.setBounds(25, 25, 200, 20);
		// Añadir el componente al panel
		contentPane.add(etiqueta1);

		// Crear un componente JTextArea
		textArea = new JTextArea();
		// Colocar el componente (x, y, longitud, altura)
		textArea.setBounds(25, 55, 200, 200);
		// Formato del texto
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		// Añadir el componente al panel
		contentPane.add(textArea);

		// Hacer la ventana visible
		setVisible(true);

	}

	@Override
	public void windowOpened(WindowEvent e) {
		textArea.append("windowOpened\n");
	}

	@Override
	public void windowClosing(WindowEvent e) {
		textArea.append("windowClosing\n");
	}

	@Override
	public void windowClosed(WindowEvent e) {
		textArea.append("windowClosed\n");
	}

	@Override
	public void windowIconified(WindowEvent e) {
		textArea.append("windowIconified\n");
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		textArea.append("windowDeiconified\n");
	}

	@Override
	public void windowActivated(WindowEvent e) {
		textArea.append("windowActivated\n");
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		textArea.append("windowDeactivated\n");
	}

}
