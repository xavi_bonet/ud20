package com.c4.maven.ud20_maven.Ejercicio1;
/*
 * @author David
 * 
 */

import javax.swing.*;

public class Ventana extends JFrame{
	private JPanel contentPane; //Panel de la aplicaci�n
	
	public Ventana() {
		//Titulo de la ventana //Cordenadas x,y,longitud,altura //Accion por defecto al cerrar la ventana
		setTitle("ejercicio 1"); 		
		setBounds(400,200,450,220); 				
		setDefaultCloseOperation(EXIT_ON_CLOSE); 	
		
		//Creamos el panel, indicamos su diseño y lo asignamos a la ventana.
		contentPane = new JPanel();
		contentPane.setLayout(null);
		setContentPane(contentPane); 
		
		//Creamos una etiqueta y la añadimos al panel.
		JLabel etiqueta = new JLabel("Hola Mundo!");
		etiqueta.setBounds(60,20,150,20);
		contentPane.add(etiqueta);
		
		//Hacemos visible la ventana
		setVisible(true); 
		
		
	}
	
	public void iniciar() {
		System.out.println("Hola Mundo!");
	}
	
	

}
