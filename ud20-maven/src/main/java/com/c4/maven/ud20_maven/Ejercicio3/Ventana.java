package com.c4.maven.ud20_maven.Ejercicio3;
/*
 * @author David
 * 
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Ventana extends JFrame{
	private JPanel contentPane; //Panel de la aplicaci�n
	private int pls1 = 0;
	private int pls2 = 0;
	
	public Ventana() {
		//Titulo de la ventana //Cordenadas x,y,longitud,altura //Accion por defecto al cerrar la ventana
		setTitle("ejercicio 3"); 		
		setBounds(400,200,450,220); 				
		setDefaultCloseOperation(EXIT_ON_CLOSE); 	
		
		//Creamos el panel, indicamos su diseño y lo asignamos a la ventana.
		contentPane = new JPanel();
		contentPane.setLayout(null);
		setContentPane(contentPane); 
		
		//Creamos un componente y lo añadimos a la ventana
		JLabel et1 = new JLabel("Boton 1: "+pls1+" veces");
		et1.setBounds(60,20,150,20);
		contentPane.add(et1);
		
		//Creamos un componente y lo añadimos a la ventana
		JLabel et2 = new JLabel("Boton 2: "+pls2+" veces");
		et2.setBounds(210,20,150,20);
		contentPane.add(et2);
		
		//Creamos un boton y lo añadimos a la ventana
		JButton btn1 = new JButton("1");
		btn1.setBounds(60,50,150,20);
		contentPane.add(btn1);
		
		//Creamos un boton y lo añadimos a la ventana
		JButton btn2 = new JButton("2");
		btn2.setBounds(210,50,150,20);
		contentPane.add(btn2);
		
		//Hacemos visible la ventana
		setVisible(true); 
		
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pls1++;
				et1.setText("Boton 1: "+pls1+" veces");
			}
		});
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pls2++;
				et2.setText("Boton 2: "+pls2+" veces");
			}
		});
		
		
	}
	
	public void iniciar() {
		System.out.println("Hola Mundo!");
	}
	
	

}
