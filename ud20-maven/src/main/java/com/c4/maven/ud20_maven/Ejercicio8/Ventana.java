package com.c4.maven.ud20_maven.Ejercicio8;
/*
 * @author David
 * 
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Ventana extends JFrame{
	
	private boolean flag = true;
	private JPanel contentPane; //Panel de la aplicaci�n
	
	public Ventana() {
		//Titulo de la ventana //Cordenadas x,y,longitud,altura //Accion por defecto al cerrar la ventana
		setTitle("ejercicio 7"); 		
		setBounds(400,200,450,220); 				
		setDefaultCloseOperation(EXIT_ON_CLOSE); 	
		
		//Creamos el panel, indicamos su diseño y lo asignamos a la ventana.
		contentPane = new JPanel();
		contentPane.setLayout(null);
		setContentPane(contentPane); 
		
		//Creamos un componente y lo añadimos a la ventana
		JLabel et1 = new JLabel("Cantidad a convertir");
		et1.setBounds(25,20,150,20);
		contentPane.add(et1);
		
		//Creamos un componente y lo añadimos a la ventana
		JLabel et2 = new JLabel("Resultado");
		et2.setBounds(25,64,100,20);
		contentPane.add(et2);
		
		//Creamos un campo de texto y lo añadimos a la ventana
		JTextField tF1 = new JTextField();
		tF1.setBounds(25,42,100,20);
		contentPane.add(tF1);
		
		//Creamos un campo de texto y lo añadimos a la ventana
		JTextField tfRes = new JTextField();
		tfRes.setBounds(25,86,200,20);
		tfRes.setEditable(false);
		contentPane.add(tfRes);
		
		
		//Creamos un boton y lo añadimos a la ventana
		JButton btnCalcular = new JButton("Euros a Ptas");
		btnCalcular.setBounds(25,130,115,20);
		contentPane.add(btnCalcular);
		
		//Creamos un boton y lo añadimos a la ventana
		JButton btnCambiar = new JButton("Cambiar");
		btnCambiar.setBounds(150,130,89,20);
		contentPane.add(btnCambiar);
		
		//Creamos un boton y lo añadimos a la ventana
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.setBounds(250,130,89,20);
		contentPane.add(btnBorrar);
		
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(flag) {
					try {
						String sop1 = tF1.getText();
						double op1 = Double.parseDouble(sop1);
						String res = " " + op1 * 166.386;
						tfRes.setText(res);
			        } catch (Exception ex) {
			        	JOptionPane.showMessageDialog(null, "Error: Formato erróneo");
			            System.out.println("Error: " + ex);
			        }
				}else {
					try {
						String sop1 = tF1.getText();
						double op1 = Double.parseDouble(sop1);
						String res = " " + op1 / 166.386;
						tfRes.setText(res);
			        } catch (Exception ex) {
			        	JOptionPane.showMessageDialog(null, "Error: Formato erróneo");
			            System.out.println("Error: " + ex);
			        }
					
				}
			}
		});
		
		btnCambiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(flag) {
					btnCalcular.setText("Ptas a euros");
					flag = false;
				}else {
					btnCalcular.setText("Euros a ptas");
					flag = true;
					
				}
			}
		});
		
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tfRes.setText("");
				tF1.setText("");
			}
		});
		
		//Hacemos visible la ventana
		setVisible(true); 

		
	}
	
	public void iniciar() {
		System.out.println("Hola Mundo!");
	}


	

}
